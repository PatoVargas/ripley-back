var express = require("express");
var https = require("https");
var redis = require('redis');
var router = express.Router();

class MinProduct{

  constructor(id , sku, name, original_price, offer_price, card_price, full_image){
    this.id = id;
    this.sku = sku;
    this.name = name;
    this.original_price = original_price;
    this.offer_price = offer_price;
    this.card_price = card_price;
    this.full_image = full_image;
  }
}

const initial_skus = ["MPM00002218952", "2000368711752P", "2000374575638P", "MPM00000096356", "2000374587280", "2000372627506",
                     "2000338145938P", "MPM00004438714", "2000327636874P", "MPM00002279928", "2000365334534P", "2000363428303P",
                     "283061", "2000369989679P", "2000376762722P", "205990", "2000362851881P", "399187", "390397", "2000375722154P",
                     "2000375921595", "2000371740336P", "2000365113832P", "2000373703803", "2000372107077P"];

const products = [];

const createObject = (data) => {
  let product = new MinProduct(data.uniqueID, data.partNumber, data.name, data.prices.formattedListPrice,
          data.prices.formattedOfferPrice, data.prices.formattedCardPrice, data.fullImage)
  return product;
}

const getOneProduct = (sku, callback) => {
  var client = redis.createClient(process.env.REDIS_URL);

  client.on('connect', () => {
    client.get(sku, (error, result) => {
      if (error) {
          throw error;
      }
      if (result) {
        const parsedData = JSON.parse(result);
        callback(parsedData);
      } else {
        https.get('https://simple.ripley.cl/api/v2/products/'+ sku, (res) => {
          const { statusCode } = res;
          const contentType = res.headers['content-type'];
          res.setEncoding('utf8');
          let rawData = '';
          res.on('data', (chunk) => { rawData += chunk; });
          res.on('end', () => {
            try {
              const parsedData = JSON.parse(rawData);
              if (parsedData.error) {
                callback(parsedData);
              }
              else{
                client.set(sku, rawData, 'EX', 60, () =>{
                  redis.print;
                  callback(parsedData);
                });
              }
            } catch (e) {
              callback(e);
            }
          });
        }).on('error', (e) => {
          console.error(`Got error: ${e.message}`);
          callback(null);
        });
      }
    });
  });
}

for (var i = 0; i < initial_skus.length; i++) {
  https.get('https://simple.ripley.cl/api/v2/products/'+ initial_skus[i], (res) => {
    const { statusCode } = res;
    const contentType = res.headers['content-type'];

    res.setEncoding('utf8');
    let rawData = '';
    res.on('data', (chunk) => { rawData += chunk; });
    res.on('end', () => {
      try {
        const parsedData = JSON.parse(rawData);
        let product = createObject(parsedData);
        products.push(product)
      } catch (e) {
        console.error(e.message);
      }
    });
  }).on('error', (e) => {
    console.error(`Got error: ${e.message}`);
  });
}

/**
  * @desc get all products
  * @param standar inputs
  * @return array with products
*/
router.get("/", function(req, res, next) {
  res.send(products);
});

/**
  * @desc get one product
  * @param standar inputs and id product
  * @return one product or error
*/
router.get("/:id", function(req, res, next) {
  var number = Math.trunc(Math.random() * (9) + 1)
  if (number == 1) {
    var error = { error: {message: 'Error random'} }
    res.send(error);
  }
  else{
    getOneProduct(req.params.id, function(product) {
      res.send(product);
    });
  }
});

module.exports = router;
